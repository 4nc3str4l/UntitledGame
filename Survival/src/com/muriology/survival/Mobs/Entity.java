package com.muriology.survival.Mobs;

import java.awt.Rectangle;

import com.muriology.survival.Graphics.Screen;
import com.muriology.survival.Graphics.Sprite;

public abstract class Entity {
	
	public Sprite sprite;

	private Rectangle rect;
	
	public Entity(int x, int y, int w, int h, Sprite sprite){
		this.rect = new Rectangle(x, y,w,h);
		this.sprite = sprite;
	}
	
	public int getX(){
		return this.rect.x;
	}
	
	public int getY(){
		return this.rect.y;
	}
	
	public int getWidth(){
		return this.rect.width;
	}
	
	public int getHeight(){
		return this.rect.height;
	}
	
	public void setX(int x){
		this.rect.x = x;
	}
	
	public void setY(int y){
		this.rect.y = y;
	}
	public abstract void tick();
	public abstract void render(Screen screen);
	public abstract void tick(int x, int y);
}
