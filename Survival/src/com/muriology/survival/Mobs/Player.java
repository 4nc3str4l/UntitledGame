package com.muriology.survival.Mobs;

import java.awt.Rectangle;

import com.muriology.survival.Graphics.Screen;
import com.muriology.survival.Graphics.Sprite;



public class Player {
	
	public static enum Direction{
		UP,
		DOWN,
		LEFT,
		RIGHT
	}
	
	private Rectangle rect;
	public Direction dir,lastDirection;
	Sprite actualSprite;
	boolean flip, alternancy;
	int upsCounter;
	
	public Player(int x, int y, int w, int h){
		this.rect = new Rectangle(x, y,w,h);
		this.actualSprite = Sprite.player_down;
		this.dir = Direction.DOWN;
		this.upsCounter = 0;
		this.alternancy = false;
	}
	
	public void tick(int x, int y) {
		//if we are moving update
		if(this.getX() != x || this.getY() != y) this.upsCounter++;
		else{//if we are static and looking at left or right i put the idle sprite as actual sprite.
			if(this.dir == Direction.RIGHT || this.dir == Direction.LEFT) this.actualSprite = Sprite.player_right;
			return;
		}
		this.flip = false;
		this.setX(x);
		this.setY(y);
		
		if(this.upsCounter == 16) 
			this.upsCounter = 0;///reset the upscounter

		if(this.upsCounter > 8) this.alternancy = true;
		else this.alternancy = false;
		
		
		switch(this.dir){
		case UP:
			this.actualSprite = Sprite.player_up;
			if(this.alternancy) this.flip = true;
			break;
		case DOWN:
			this.actualSprite = Sprite.player_down;
			if(this.alternancy) this.flip = true;
			break;
		case LEFT:
			this.flip = true;
			if(this.alternancy){
				this.actualSprite = Sprite.player_right;
			}else{
				this.actualSprite = Sprite.player_right_2;
			}
			break;
		case RIGHT:
			if(this.alternancy){
				this.actualSprite = Sprite.player_right;
			}else{
				this.actualSprite = Sprite.player_right_2;
			}
			break;
		default:
			break;
		}
	}

	public void render(Screen screen) {
		screen.renderPlayer(this.getWidth(), this.getHeight(),this.actualSprite, this.flip);
	}

	
	public int getX(){
		return this.rect.x;
	}
	
	public int getY(){
		return this.rect.y;
	}
	
	public int getWidth(){
		return this.rect.width;
	}
	
	public int getHeight(){
		return this.rect.height;
	}
	
	public void setX(int x){
		this.rect.x = x;
	}
	
	public void setY(int y){
		this.rect.y = y;
	}
}
