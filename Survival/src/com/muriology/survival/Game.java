package com.muriology.survival;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

import javax.swing.JFrame;

import com.muriology.survival.Graphics.Font;
import com.muriology.survival.Graphics.GUI;
import com.muriology.survival.Graphics.Screen;
import com.muriology.survival.Input.Input;
import com.muriology.survival.Mobs.Player;
import com.muriology.survival.map.Enviroment;
import com.muriology.survival.map.Map;

public class Game extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "Untitled Game";
	public static final int HEIGHT = 120;
	public static final int WIDTH = 160;
	public static final int SCALE = 3;
	public static String RES_PATH = "../../../../res/";
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	
	private boolean running;
	
	private Screen screen;
	private Input input;
	public GUI gui;
	
	public Random random;
	
	private Map map;
	private Player player;
	private Enviroment enviroment;
		
	int x, y;
	
	public Game(){
		this.running = false;
		this.input = new Input();
		this.screen = new Screen(this.WIDTH, this.HEIGHT, this);
		this.random = new Random();
		this.map = new Map(100, 100, 8, 8);
		this.x = 0;
		this.y = 0;
		this.gui = new GUI(0,110, 160,10);
		this.enviroment = new Enviroment(0);
		this.player = new Player(0 , 0 , 16 , 16);
		addKeyListener(input);
	}
	
	/**
	 * Starts our main thread.
	 */
	public synchronized void start(){
		running = true;
		new Thread(this).start();

	}
	/**
	 * Stops our main thread
	 */
	public synchronized void stop(){
		this.running = false;
	}
	
	@Override
	public void run() {
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0/60;
		double delta = 0;
		int frames = 0;
		int updates = 0;
		requestFocus();
		while(running){
			long now = System.nanoTime();
			delta += (now-lastTime) /ns;
			lastTime = now;
			while(delta >= 1){//60 times per sec
				tick();
				updates++;
				delta--;
			}
			render();//as fast as i can
			frames ++;
			if(System.currentTimeMillis() -timer > 1000){
				timer += 1000;
				System.out.println("Fps:"+frames+" Updates:"+updates);
				updates = 0;
				frames = 0;
			}
		}
	}
	
	public void renderString(String string, int posX, int posY){
		
		boolean space;
		string = string.toLowerCase();
		String s = "";
		int off = 8;
		int cursor_pos = posX;
		int actual_line = posY;
		int max_width = screen.getWidth() / (off);

		for(int i = 0;i<string.length();i++){
			space = false;

			if(i > 0) cursor_pos++;

			
			s = string.substring(i, i+1);
			switch(s){
			case "a":
				Font.a.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "b":
				Font.b.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "c":
				Font.c.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "d":
				Font.d.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "e":
				Font.e.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "f":
				Font.f.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "g":
				Font.g.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "h":
				Font.h.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "i":
				Font.i.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "j":
				Font.j.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "k":
				Font.k.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "l":
				Font.l.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "m":
				Font.m.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "n":
				Font.n.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "o":
				Font.o.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "p":
				Font.p.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "q":
				Font.q.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "r":
				Font.r.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "s":
				Font.s.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "t":
				Font.t.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "u":
				Font.u.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "v":
				Font.v.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "w":
				Font.w.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "x":
				Font.x.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "y":
				Font.y.render(cursor_pos*off,actual_line*off, screen);
				break;
			case "z":
				Font.z.render(cursor_pos*off,actual_line*off, screen);
				break;
			default:
				if(s.equals(" ")) space = true;
				if(space && cursor_pos == posX) cursor_pos--;
				else if(s.equals("�")) cursor_pos = max_width-1;
				break;
			}
			if(cursor_pos == max_width-1){
				actual_line++;
				cursor_pos = posX-1;
				
			}
		}
		
	}
	
	public void tick(){
		input.tick();
		if(input.right  && x < (this.map.getW() * this.map.getTileWidth()- this.WIDTH)){
			this.x++;
			this.player.dir = Player.Direction.RIGHT;
		}
		if(input.left   && this.x > 0){
			this.x--;
			this.player.dir = Player.Direction.LEFT;
		}
		if(input.down	&& this.y < (this.map.getH() * this.map.getTileHeight()) - this.HEIGHT){
			this.y++;
			this.player.dir = Player.Direction.DOWN;
		}
		if(input.up 	&& this.y > 0){
			this.y--;
			this.player.dir = Player.Direction.UP;
		}
		player.tick(x, y);
		enviroment.tick();
	}
	
	public void render(){
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			requestFocus();
			return;
		}
		
		//All graphics here
		
		this.screen.clear();
		this.map.render(screen, x,  y);
		this.enviroment.render(screen);
		this.player.render(screen);
		this.gui.render(screen);
		this.renderString("Test text", 0, 0);
		for(int i = 0;i<this.screen.getPixels().length;i++){
			this.pixels[i] = this.screen.getPixel(i);
		}
		
		Graphics g = bs.getDrawGraphics();
		g.drawImage(image,0,0,getWidth(),getHeight(),null);
		g.dispose();
		bs.show();
	}
	
	public static void main(String[]args){
		Game game = new Game();
		game.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		game.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		game.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		
		JFrame frame = new JFrame(Game.NAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(game , BorderLayout.CENTER);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		game.start();
	}
}