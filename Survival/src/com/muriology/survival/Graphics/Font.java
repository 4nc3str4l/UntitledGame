package com.muriology.survival.Graphics;

public class Font {
	
	
	public String letter;
	private Sprite sprite;
	
	public static Font a = new Font("a", Sprite.a_sprite);
	public static Font b = new Font("b", Sprite.b_sprite);
	public static Font c = new Font("c", Sprite.c_sprite);
	public static Font d = new Font("d", Sprite.d_sprite);
	public static Font e = new Font("e", Sprite.e_sprite);
	public static Font f = new Font("f", Sprite.f_sprite);
	public static Font g = new Font("g", Sprite.g_sprite);
	public static Font h = new Font("h", Sprite.h_sprite);
	public static Font i = new Font("i", Sprite.i_sprite);
	public static Font j = new Font("j", Sprite.j_sprite);
	public static Font k = new Font("k", Sprite.k_sprite);
	public static Font l = new Font("l", Sprite.l_sprite);
	public static Font m = new Font("m", Sprite.m_sprite);
	public static Font n = new Font("n", Sprite.n_sprite);
	public static Font o = new Font("o", Sprite.o_sprite);
	public static Font p = new Font("p", Sprite.p_sprite);
	public static Font q = new Font("q", Sprite.q_sprite);
	public static Font r = new Font("r", Sprite.r_sprite);
	public static Font s = new Font("s", Sprite.s_sprite);
	public static Font t = new Font("t", Sprite.t_sprite);
	public static Font u = new Font("u", Sprite.u_sprite);
	public static Font v = new Font("v", Sprite.v_sprite);
	public static Font w = new Font("w", Sprite.w_sprite);
	public static Font x = new Font("x", Sprite.x_sprite);
	public static Font y = new Font("y", Sprite.y_sprite);
	public static Font z = new Font("z", Sprite.z_sprite);
	
	public Font(String letter, Sprite sprite){
		this.letter = letter;
		this.sprite = sprite;
	}
	
	public void render(int x, int y,Screen screen){
		screen.renderFont(x, y, sprite);
	}
}
