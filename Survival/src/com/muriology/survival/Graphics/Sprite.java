package com.muriology.survival.Graphics;

public class Sprite {
	
	public int[] pixels;
	private int w,h;
	
	public static Sprite grass = new Sprite(2,0,8,8,SpriteSheet.spriteSheet);
	
	public static Sprite player_down = new Sprite(0,14,16,16,SpriteSheet.spriteSheet);
	public static Sprite player_down_up = new Sprite(0,15,16,16,SpriteSheet.spriteSheet);
	
	public static Sprite player_up = new Sprite(2,14,16,16,SpriteSheet.spriteSheet);
	public static Sprite player_up_up = new Sprite(2,15,16,16,SpriteSheet.spriteSheet);
	
	public static Sprite player_right = new Sprite(4,14,16,16,SpriteSheet.spriteSheet);
	public static Sprite player_right_up = new Sprite(4,15,16,16,SpriteSheet.spriteSheet);
	
	public static Sprite player_right_2 = new Sprite(6,14,16,16,SpriteSheet.spriteSheet);
	public static Sprite player_right_up_2 = new Sprite(6,15,16,16,SpriteSheet.spriteSheet);
	
	public static Sprite a_sprite = new Sprite(0, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite b_sprite = new Sprite(1, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite c_sprite = new Sprite(2, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite d_sprite = new Sprite(3, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite e_sprite = new Sprite(4, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite f_sprite = new Sprite(5, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite g_sprite = new Sprite(6, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite h_sprite = new Sprite(7, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite i_sprite = new Sprite(8, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite j_sprite = new Sprite(9, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite k_sprite = new Sprite(10, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite l_sprite = new Sprite(11, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite m_sprite = new Sprite(12, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite n_sprite = new Sprite(13, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite o_sprite = new Sprite(14, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite p_sprite = new Sprite(15, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite q_sprite = new Sprite(16, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite r_sprite = new Sprite(17, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite s_sprite = new Sprite(18, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite t_sprite = new Sprite(19, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite u_sprite = new Sprite(20, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite v_sprite = new Sprite(21, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite w_sprite = new Sprite(22, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite x_sprite = new Sprite(23, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite y_sprite = new Sprite(24, 30, 8, 8, SpriteSheet.spriteSheet);
	public static Sprite z_sprite = new Sprite(25, 30, 8, 8, SpriteSheet.spriteSheet);
	
	
	
	public Sprite(int x, int y, int w, int h, SpriteSheet spriteSheet){
		this.w = w;
		this.h = h;
		
		this.pixels = new int[this.w * this.h];
		int xOrg = x * 8;
		int yOrg = y * 8;
		for(int i = 0; i < h; i++){
			for(int j = 0; j < w; j++){
				this.pixels[j + i * w] = spriteSheet.pixels[(xOrg + j) + (yOrg + i)* spriteSheet.width];
			}
		}
	}
	
	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public void render(int x, int y, int xPos, int yPos, Screen screen, int color){
		screen.renderSprite(x , y,this.w,this.h, pixels, xPos, yPos, color);
	}

}
