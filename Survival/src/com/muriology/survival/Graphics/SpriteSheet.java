package com.muriology.survival.Graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.muriology.survival.Game;

public class SpriteSheet {
	
	public String path;
	public int[] pixels;
	int width;
	
	public static SpriteSheet spriteSheet  = new SpriteSheet(Game.RES_PATH+"spriteSheet.png");
	
	public SpriteSheet(String path){
		this.path = path;
		System.out.println(path);
		this.load();
	}
	
	private void load(){
		BufferedImage image;
		try{	
			image = ImageIO.read(SpriteSheet.class.getResource(path));
			int w = image.getWidth();
			int h = image.getHeight();
			this.width = w;
			this.pixels = new int[w * h];
			
			image.getRGB(0, 0, w, h, pixels, 0, w);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
