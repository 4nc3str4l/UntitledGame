package com.muriology.survival.Graphics;

public class GUI {
	private int x, y, w ,h;
	
	public GUI(int x,int y,int w, int h){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public void render(Screen screen){
		screen.renderGUI(x,y,w,h);
	}
	
	public void tick(){
		
	}
}
