package com.muriology.survival.Graphics;

public class Tile {
	
	public int x, y, width, height, color;
	Sprite sprite;
	
	public Tile(int x, int y, int w, int h, Sprite sprite, int color){
		this.x = x; 
		this.y = y;
		this.width = w;
		this.height = h;
		this.sprite = sprite;
		this.color = color;
	}
	
	public void render(Screen screen, int xPos, int yPos){
		screen.renderSprite(this.x, this.y, this.width, this.height, this.sprite.pixels, xPos, yPos, this.color);
	}
}
