package com.muriology.survival.Graphics;

import com.muriology.survival.Game;


public class Screen {
	
	private int width;
	private int height;
	private int[] pixels;
	private Game game;
	
	public Screen(int width, int height,Game game){
		
		this.width = width;
		this.height = height;
		this.game = game;
		
		this.pixels = new int[this.width * this.height]; 
	}

	public void clear(){
		for(int i = 0;i < this.pixels.length;i++){
			this.pixels[i] = 0;
		}
	}
	
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int[] getPixels() {
		return pixels;
	}

	public void setPixels(int[] pixels) {
		this.pixels = pixels;
	}
	
	public int getPixel(int i){
		return this.pixels[i];
	}

	public void renderTile(int x, int y, int width, int height, int color, int xPos, int yPos) {
		y = y-yPos;
		x = x-xPos;
		for(int i = y; i < y + height; i++){
			if(i >= 0 && i < this.height){ //If i'm inside the map
				for(int j = x; j < x + width; j++){
					if(j >= 0 && j<this.width){
						this.pixels[j + i * this.width] = color;
					}
				}
			}
		}
	}

	public void renderSprite(int x, int y, int w, int h, int[] pixels2,  int xPos, int yPos, int color) {
		y = y-yPos;
		x = x-xPos;
		for(int i = y; i < y + h; i++){
			if(i >= 0 && i < this.height){ //If i'm inside the map
				for(int j = x; j < x + w; j++){
					if(j >= 0 && j < this.width){
						this.pixels[j + i * this.width] = pixels2[(j - x) + (i - y) * w] + color;
					}
				}
			}
		}
	}
	
	public void renderGUI(int x, int y, int w, int h) {
		for(int i = y; i < y + h; i++){
			if(i >= 0 && i < this.height){ //If i'm inside the map
				for(int j = x; j < x + w; j++){
					if(j >= 0 && j<this.width){
						if(i == y){
							this.pixels[j + i * this.width] = 0xFFFFFF;
						}else{
							this.pixels[j + i * this.width] = 0;
						}
						
					}
				}
			}
		}
	}

	

	public void renderPlayer(int width2, int height2, Sprite sprite, boolean flip) {
		for(int i = 0; i < height2; i++){
			for(int j = 0; j < width2; j++){
				if(sprite.pixels[j + width2 * i] != -16777216)
					if(flip){
						this.pixels[ (sprite.getW() + this.width/2-width2/2 - j) + (i - height2/2 -2 + this.height/2) * this.width] = sprite.pixels[j + sprite.getW() * i];
					}else{
						this.pixels[ (j + this.width/2-width2/2) + (i - height2/2 + this.height/2 - 2) * this.width] = sprite.pixels[j + sprite.getW() * i];
					}
				}
		}
	}

	public void renderTile(int x, int y, int width2, int height2, Sprite sprite, int xPos, int yPos, int color) {
		this.renderSprite(x, y, width2, height2, sprite.pixels, xPos, yPos, color);
	}

	public void renderGlobalIlumination(int ilumination) {
		int x = 112;
		int y = 106;
		int r = 30 + game.random.nextInt(2);
		if(ilumination == -1){
			
			for(int i = 0; i < this.pixels.length;i++){
				this.pixels[i] -= 0x00444444;
			}	
			
			x -= this.width/2/2 - 8;
			y -= this.height/2 - 8;
			int x0 = x - r*3;
			int x1 = x + r*3;
			int y0 = y - r*3;
			int y1 = y + r*3;

			if (x0 < 0) x0 = 0;
			if (y0 < 0) y0 = 0;
			if (x1 > this.getWidth()) x1 = this.getWidth();
			if (y1 > this.getHeight()) y1 = this.getHeight();
			// System.out.println(x0 + ", " + x1 + " -> " + y0 + ", " + y1);
			for (int yy = y0; yy < y1; yy++) {
				int yd = yy - y;
				yd = yd * yd;
				for (int xx = x0; xx < x1; xx++) {
					int xd = xx - x;
					int dist = xd * xd + yd;
					// System.out.println(dist);
					if (dist <= r * r) {
						int br = (255 - dist * 255 / (r * r * 1) + this.pixels[xx + yy * this.getWidth()]/2*1  );
						if (pixels[xx + yy * this.getWidth()] < br) pixels[xx + yy * this.getWidth()] = br;
					}else{
						pixels[xx + yy * this.getWidth()] = 0;
					}
				}
			}
			
		}else{
			for(int i = 0; i < this.pixels.length;i++){
				this.pixels[i] -= ilumination;
			}	
		}
	}

	public void renderFont(int x, int y, Sprite sprite) {
		for(int i = 0;i<sprite.getH();i++){
			int yOffset = y + i;
			for(int j = 0; j<sprite.getW();j++){
				int xOffset = j + x;
				if(sprite.pixels[j + sprite.getW() * i] != -16777216)
					this.pixels[xOffset + yOffset * this.width] = sprite.pixels[j + i * sprite.getW()];
			}
		}
	}
}
