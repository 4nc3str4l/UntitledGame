package com.muriology.survival.map;
import java.util.Random;

import com.muriology.survival.Graphics.Screen;
import com.muriology.survival.Graphics.Sprite;
import com.muriology.survival.Graphics.Tile;

public class Map {
	
	Tile[] tile;
	int w, h, tileWidth, tileHeight;
	Random random; 
	
	public Map(int w, int h, int tileWidth, int tileHeight){
		this.w = w;
		this.h = h;
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
		tile = new Tile[w * h];
		this.generateRandomMap();

	}
	
	public void generateRandomMap(){
		this.random = new Random();
		for(int i = 0; i < this.h; i++){
			for(int j = 0; j < this.h; j++){
				this.tile[j + i * this.w] = new Tile(j * this.tileWidth, i * this.tileHeight, this.tileWidth, this.tileHeight ,Sprite.grass, (0x004400 + this.random.nextInt(20)));
			}
		}
	}
	
	public void render(Screen screen, int x, int y){
		for(int i = 0;i<this.tile.length;i++) 
			this.tile[i].render(screen, x, y);
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public int getTileWidth() {
		return tileWidth;
	}

	public void setTileWidth(int tileWidth) {
		this.tileWidth = tileWidth;
	}

	public int getTileHeight() {
		return tileHeight;
	}

	public void setTileHeight(int tileHeight) {
		this.tileHeight = tileHeight;
	}
		
}
