package com.muriology.survival.map;

import com.muriology.survival.Graphics.Screen;

public class Enviroment {
	
	int time;
	int ilumination;
	
	public Enviroment(int time){
		this.time = time;
	}
	
	public void tick(){
		this.time++;
		if(this.time < 2000){
			this.ilumination = 0;
		}else if(this.time < 3000){
			this.ilumination = 0x00444444;
		}else if(this.time < 4000){
			this.ilumination = -1;
		}else if(this.time < 5000){
			this.ilumination = 0x00444444;
		}else{
			time = 0;
		}
	}
	
	public void render(Screen screen){
		screen.renderGlobalIlumination(this.ilumination);
	}
}
